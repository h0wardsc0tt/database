USE [HME]
GO

/****** Object:  Table [ImagePortal].[categories]    Script Date: 8/17/2016 1:32:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [ImagePortal].[categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryGuid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_categories_CategoryGuid]  DEFAULT (newid()),
	[CategoryName] [varchar](100) NOT NULL,
	[CategoryDescription] [varchar](250) NULL,
	[CategorySort] [smallint] NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF__categorie__IsAct__5AB9788F]  DEFAULT ((1)),
	[CreateDTS] [datetime] NOT NULL CONSTRAINT [DF__categorie__Creat__5BAD9CC8]  DEFAULT (getdate()),
 CONSTRAINT [PK__categori__19093A2BAA2A6FD8] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


