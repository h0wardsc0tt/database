USE [HME]
GO

/****** Object:  Table [dbo].[ImagePortal.link_types]    Script Date: 8/17/2016 1:33:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ImagePortal.link_types](
	[ImagePrefix] [nvarchar](4) NOT NULL,
	[LinkDescription] [nvarchar](50) NOT NULL,
	[ImageType] [nvarchar](2) NOT NULL
) ON [PRIMARY]

GO


