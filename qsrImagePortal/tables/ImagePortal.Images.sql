USE [HME]
GO

/****** Object:  Table [ImagePortal].[images]    Script Date: 8/17/2016 1:31:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [ImagePortal].[images](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_images_Guid]  DEFAULT (newid()),
	[Product] [int] NOT NULL,
	[ProductOrder] [int] NULL,
	[ImageType] [int] NOT NULL,
	[ImageAnchorText] [varchar](150) NULL,
	[ImageName] [varchar](50) NOT NULL,
	[ImageDescription] [varchar](250) NOT NULL,
	[ImageSort] [tinyint] NOT NULL,
	[Image] [varbinary](max) NOT NULL,
	[ImagePrefix] [nchar](4) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_images_IsActive]  DEFAULT ((1)),
	[CreateDTS] [datetime] NOT NULL CONSTRAINT [DF__images__CreateDTS__5F7E2DAC]  DEFAULT (getdate()),
 CONSTRAINT [PK__images__6F0F989FB1D43767] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


