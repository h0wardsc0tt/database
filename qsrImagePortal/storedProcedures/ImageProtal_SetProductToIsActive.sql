USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetProductToIsActive]    Script Date: 8/21/2016 3:05:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetProductToIsActive](
	@categoryGuid NVARCHAR(150),
	@product INT
)AS
DECLARE @productOrder INT
DECLARE @categoryId INT

SELECT @categoryId = categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid

SELECT @productOrder = ISNULL(max(productOrder) + 1, 1)
FROM ImagePortal.images WHERE  isActive = 1 and imageType != 1 and categoryId = @categoryId

UPDATE imageportal.images SET product = @productOrder, productOrder = @productOrder, isActive = 1 where product = @product and categoryid = @categoryId