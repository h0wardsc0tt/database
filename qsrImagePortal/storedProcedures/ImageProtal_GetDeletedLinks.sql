USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_GetDeletedLinks]    Script Date: 8/21/2016 3:01:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_GetDeletedLinks](
	@categoryGuid NVARCHAR(150),
	@productId int
)AS

SELECT product, imageType, imageAnchorText, imageDescription, ImageName, guid 
	FROM imageportal.images 
	WHERE 
		categoryId = (SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
		AND product = @productId 
		AND isActive = 0
		AND imageType > 2
	ORDER BY imageSort