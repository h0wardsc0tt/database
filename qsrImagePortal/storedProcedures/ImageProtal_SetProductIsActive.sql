USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetProductIsActive]    Script Date: 8/21/2016 3:05:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetProductIsActive](
	@guid NVARCHAR(150)
)AS

DECLARE @categoryId INT
DECLARE @product INT
DECLARE @productOrder INT

SELECT @product = product, @categoryId = CategoryId FROM  imagePortal.images WHERE guid = @guid


SELECT @productOrder = ISNULL(max(product) + 1, 1)
FROM ImagePortal.images WHERE  imageType != 1 and categoryId = @categoryId

UPDATE imagePortal.images SET productOrder =  @productOrder, product = @productOrder, isActive = 0 WHERE product = @product and categoryId = @categoryId AND imageType > 1

DECLARE @productRows TABLE (product  INT)
DECLARE @productRowsUpdate TABLE (product  INT, productOrder INT)
DECLARE @productID int

SET @productOrder = 1

INSERT INTO @productRows
	SELECT product
		FROM imageportal.images 
		WHERE 
			categoryId = @categoryID
			AND isActive = 1 AND imageType != 1
		GROUP BY product
		ORDER BY product

WHILE (SELECT COUNT(*) FROM @productRows) > 0
BEGIN
	SELECT TOP 1 @productID = product
	FROM @productRows
	INSERT INTO @productRowsUpdate
	VALUES (@productID ,@productOrder)

	SET @productOrder = @productOrder + 1
	DELETE FROM @productRows WHERE product = @productID
END

UPDATE i 
SET productOrder = t.productOrder,  product = t.productOrder
FROM imageportal.images i, @productRowsUpdate t
WHERE i.product = t.product AND i.categoryid= @categoryId AND i.isActive = 1 and imageType != 1