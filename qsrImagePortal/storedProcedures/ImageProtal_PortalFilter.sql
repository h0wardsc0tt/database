USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_PortalFilter]    Script Date: 8/21/2016 3:02:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_PortalFilter](
	@productDescriptionFilter NVARCHAR(50)
)AS

DECLARE @images TABLE
(
	imageId INT,
    categoryID INT, 
	guid UNIQUEIDENTIFIER,
	product INT,
	productOrder INT,
	imagetype INT,
	imageanchortext nvarchar(150),
	imagename nvarchar(50), 
	imagedescription nvarchar(250), 
	imagesort INT,
	imageprefix nvarchar(4),
	isActive INT 
 )

 --Filter Products without including Category Image
 insert into @images
 SELECT
    imageId,
    categoryID,
    guid,
    product,
    productOrder,
    imagetype,
    imageanchortext,
    imagename,
    imagedescription,
    imagesort,
    imageprefix,
	isActive
FROM ImagePortal.images
WHERE imagedescription like '%' + @productDescriptionFilter + '%' and imagetype != 1 and isActive = 1

--Add Category Images as Required
insert into @images
select DISTINCT
    i.imageId,
    i.categoryID,
    i.guid,
    i.product,
    i.productOrder,
    i.imagetype,
    i.imageanchortext,
    i.imagename,
    i.imagedescription,
    i.imagesort,
    i.imageprefix,
	i.isActive
from ImagePortal.images i
INNER JOIN @images ii on ii.categoryID = i.CategoryId
where i.ImageType = 1

--Now perform the Select
SELECT
    c.categoryGuid, 
    c.categoryID, 
    i.product,
    c.categoryDescription, 
    c.categoryName,
    i.imageId, 
    i.imageType, 
    i.imageAnchorText, 
    i.imageName, 
    i.imageDescription, 
    i.imagePrefix, 
    i.imageSort,
    i.guid,
    (SELECT COUNT(categoryID) FROM @images WHERE categoryID = c.categoryID and imageType != 1 and product = i.product and i.isactive = 1 and isactive = 1) AS imageCount
                
FROM ImagePortal.categories c
INNER JOIN @images i on c.categoryid = i.categoryid and i.isactive = 1
WHERE c.isactive = 1
ORDER BY c.categorySort, i.product, i.productOrder, i.imagesort

