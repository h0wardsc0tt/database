USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateCategoryOrder]    Script Date: 8/21/2016 3:06:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateCategoryOrder](
	@categoryGuid NVARCHAR(150),
	@sortOrders NVARCHAR(4000)
)
WITH EXECUTE AS OWNER   
AS

DECLARE @SQLQuery AS NVARCHAR(500)

CREATE TABLE ImagePortal.temp
(
    product nvarchar(3), 
	productOrder nvarchar(3)
) 
 
SET @SQLQuery = 'INSERT INTO ImagePortal.temp  VALUES ' + @sortOrders

EXECUTE(@SQLQuery)

UPDATE c
SET categorySort= t.ProductOrder
FROM imagePortal.categories c, ImagePortal.temp t
WHERE categorySort= t.Product
 
DROP TABLE ImagePortal.temp