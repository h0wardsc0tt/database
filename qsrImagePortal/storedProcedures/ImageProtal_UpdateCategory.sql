USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateCategory]    Script Date: 8/21/2016 3:05:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateCategory](
	@categoryGuid NVARCHAR(150),
	@categoryDescription NVARCHAR(250),
	@categoryName NVARCHAR(150)
)AS

UPDATE ImagePortal.categories
SET categoryName = @categoryName, CategoryDescription = @categoryDescription
WHERE CategoryGuid = @categoryGuid
