USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateLinkOrder]    Script Date: 8/21/2016 3:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateLinkOrder](
	@categoryGuid NVARCHAR(150),
	@product INT,
	@sortOrders NVARCHAR(4000)
)
WITH EXECUTE AS OWNER   
AS

DECLARE @SQLQuery AS NVARCHAR(500)

CREATE TABLE ImagePortal.temp
(
    product INT, 
	productOrder INT
) 
 
SET @SQLQuery = 'INSERT INTO ImagePortal.temp  VALUES ' + @sortOrders

EXECUTE(@SQLQuery)

UPDATE i
SET imagesort = t.ProductOrder+1
FROM imagePortal.images i, ImagePortal.temp t
WHERE i.product = @product and imagetype > 2 and i.ImageSort =  t.product+1 and categoryid = 
	(SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
 
DROP TABLE ImagePortal.temp