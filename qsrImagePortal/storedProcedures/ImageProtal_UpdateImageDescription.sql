USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateImageDescription]    Script Date: 8/21/2016 3:07:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateImageDescription](
	@guid NVARCHAR(150),
	@imageDescription VARCHAR(250) = NULL 
)AS

UPDATE ImagePortal.images 
SET imageDescription = ISNULL(@imageDescription, imageDescription) 
WHERE guid = cast(@guid  as uniqueidentifier)


