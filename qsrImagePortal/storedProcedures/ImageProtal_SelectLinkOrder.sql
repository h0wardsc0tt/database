USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SelectLinkOrder]    Script Date: 8/21/2016 3:03:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SelectLinkOrder](
	@categoryGuid NVARCHAR(150),
	@product INT
)
AS

DECLARE @imageRows TABLE (imageid INT, product  INT, imagesort INT)
DECLARE @imageSort INT
DECLARE @imageId INT

INSERT INTO @imageRows
	SELECT imageid, product, imagesort 
		FROM imageportal.images 
		WHERE 
			product = @product  
			AND categoryId = (SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
			AND isActive = 1
			AND imageType > 2
		ORDER BY imageSort

SET @imageSort = 2

WHILE (SELECT COUNT(*) FROM @imageRows) > 0
BEGIN
	SELECT TOP 1 @imageId = imageId 
	FROM @imageRows
	UPDATE ImagePortal.images 
	SET imageSort = @imageSort 
		WHERE imageId = @imageId

	SET @imageSort = @imageSort + 1
	DELETE FROM @imageRows WHERE imageId = @imageId
END

SELECT product, imagesort AS productOrder, ImageAnchorText AS imageDescription, guid 
	FROM imageportal.images
	WHERE 
		product = @product 
		AND isActive = 1 
		AND imagetype > 2 
		AND categoryId = (SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
ORDER BY ImageSort
