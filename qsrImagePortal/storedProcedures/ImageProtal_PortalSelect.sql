USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_PortalSelect]    Script Date: 8/21/2016 3:02:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_PortalSelect]
AS

SELECT
    c.categoryGuid, 
    c.categoryID, 
    i.product,
    c.categoryDescription, 
    c.categoryName,
    i.imageId, 
    i.imagetype, 
    i.imageanchortext, 
    i.imagename, 
    i.imagedescription, 
    i.imageprefix, 
    i.image,
    i.imagesort,
    i.guid,
    (SELECT count(categoryID) FROM ImagePortal.images WHERE categoryID = c.categoryID and imageType != 1 and product = i.product and i.isactive = 1 and isactive = 1) as imageCount                
FROM ImagePortal.categories c
INNER JOIN ImagePortal.images i on c.categoryid = i.categoryid and i.isactive = 1
WHERE c.isactive = 1
ORDER BY c.categorySort, i.product, i.productOrder, i.imagesort 