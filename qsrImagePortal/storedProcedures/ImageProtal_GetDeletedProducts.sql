USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_GetDeletedProducts]    Script Date: 8/21/2016 3:02:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_GetDeletedProducts](
	@categoryGuid NVARCHAR(150)
)AS

select product, ImageDescription, guid, ImageId from ImagePortal.images
WHERE 
	categoryid = (SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
	and imagetype=2
	and isActive = 0