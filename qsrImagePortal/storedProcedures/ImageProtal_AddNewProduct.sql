USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_AddNewProduct]    Script Date: 8/21/2016 2:58:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_AddNewProduct](
	@categoryGuid NVARCHAR(150),
	@anchorText NVARCHAR(150),
	@imageName  NVARCHAR(50),
	@imageDescription NVARCHAR(250),
	@image IMAGE,
	@imagePrefix NVARCHAR(4)
)AS

DECLARE @categoryId INT
DECLARE @Product INT
DECLARE @ProductOrder INT
DECLARE @requestType NVARCHAR(20)
DECLARE @imageType  INT
DECLARE @imageSort INT

SELECT @categoryId = categoryId FROM ImagePortal.categories WHERE categoryGuid = @categoryGuid and isActive = 1
SELECT @product = ISNULL(max(product) + 1, 1)
FROM ImagePortal.images WHERE  isActive = 1 and imageType != 1 and categoryId = @categoryId

SET @productOrder = @product
SET @imageType = 2
SET @imageSort = 1

insert into ImagePortal.Images 
(
	CategoryId
	,Product
	,ProductOrder
	,ImageType
	,ImageAnchorText
	,ImageName
	,ImageDescription
	,ImageSort
	,Image
	,ImagePrefix
)

VALUES
(
	@categoryId,
	@product,
	@productOrder,
	@imageType,
	@anchorText,
	@imageName,
	@imageDescription,
	@imageSort,
	@image,
	@imagePrefix
)
