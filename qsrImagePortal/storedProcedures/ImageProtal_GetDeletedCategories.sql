USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_GetDeletedCategories]    Script Date: 8/21/2016 3:01:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_GetDeletedCategories]
AS
SELECT CategoryGuid, CategoryDescription  FROM imageportal.categories WHERE isActive = 0



