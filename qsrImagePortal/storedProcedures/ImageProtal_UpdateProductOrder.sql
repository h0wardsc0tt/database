USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateProductOrder]    Script Date: 8/21/2016 3:08:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateProductOrder](
	@categoryGuid NVARCHAR(150),
	@sortOrders NVARCHAR(4000)
)
WITH EXECUTE AS OWNER   
AS

DECLARE @SQLQuery AS NVARCHAR(500)

CREATE TABLE ImagePortal.temp
(
    product nvarchar(3), 
	productOrder nvarchar(3)
) 
 
SET @SQLQuery = 'INSERT INTO ImagePortal.temp  VALUES ' + @sortOrders

EXECUTE(@SQLQuery)

UPDATE i
SET ProductOrder = t.ProductOrder, Product = t.ProductOrder
FROM imagePortal.images i, ImagePortal.temp t
WHERE i.product = t.product  and imagetype != 1 and categoryid = 
	(SELECT categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid)
 
DROP TABLE ImagePortal.temp