USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateImageAndImageDescriptions]    Script Date: 8/21/2016 3:06:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateImageAndImageDescriptions](
	@image IMAGE = NULL,
	@guid NVARCHAR(150),
	@imageDescription VARCHAR(250) = NULL 
)AS

IF @image IS NULL 
BEGIN
	UPDATE ImagePortal.images 
	SET imageDescription = ISNULL(@imageDescription, 'imageDescription') 
	WHERE guid = cast(@guid  as uniqueidentifier)
END
ELSE
BEGIN
	UPDATE ImagePortal.images 
	SET image = @image, imageDescription = ISNULL(@imageDescription, imageDescription) 
	WHERE guid = cast(@guid  as uniqueidentifier)
END

