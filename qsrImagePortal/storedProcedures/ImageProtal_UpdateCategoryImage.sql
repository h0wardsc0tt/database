USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateCategoryImage]    Script Date: 8/21/2016 3:05:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateCategoryImage](
	@categoryGuid NVARCHAR(150),
	@categoryDescription NVARCHAR(250),
	@categoryName NVARCHAR(150),
	@image IMAGE,
	@imageName NVARCHAR(50),
	@imagePrefix NVARCHAR(4)
)AS

DECLARE @categoryId INT

SELECT @categoryId = categoryId FROM ImagePortal.categories WHERE CategoryGuid = @categoryGuid

UPDATE ImagePortal.images
SET image = @image, imageName = @imageName, imagePrefix = @imagePrefix
WHERE categoryId = @categoryId and imageType = 1

UPDATE ImagePortal.categories
SET categoryName = @categoryName, CategoryDescription = @categoryDescription
WHERE categoryId = @categoryId
