USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetLinkToActive]    Script Date: 8/21/2016 3:04:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetLinkToActive](	
	@guid NVARCHAR(150)
)AS

DECLARE @categoryId INT
DECLARE @product INT
DECLARE @imageSort INT

SELECT @categoryId = categoryId, @product = product FROM ImagePortal.images WHERE guid = @guid

SELECT @imageSort = max(imageSort) + 1
	FROM ImagePortal.images WHERE categoryId = @categoryId AND isActive = 1

UPDATE imageportal.images SET isActive = 1, product = @product, ProductOrder = @product, imageSort = @imageSort WHERE guid = @guid
