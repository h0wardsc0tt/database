USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SelectCategoryOrder]    Script Date: 8/21/2016 3:03:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SelectCategoryOrder]
AS

SELECT categoryGuid, categorySort AS productOrder, CategoryDescription FROM imageportal.categories WHERE isActive = 1
order by CategorySort
