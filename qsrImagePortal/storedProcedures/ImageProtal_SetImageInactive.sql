USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetImageInactive]    Script Date: 8/21/2016 3:04:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetImageInactive](
	@guid NVARCHAR(150)
)AS

DECLARE @categoryId INT
DECLARE @product INT
DECLARE @imageSort INT
DECLARE @imageId INT

DECLARE @linkRows TABLE (imageId  INT, product INT)
DECLARE @linkRowsUpdate TABLE (imageId  INT, product INT, imageSort INT)

SELECT @categoryId = categoryId, @product = product FROM ImagePortal.images WHERE guid = @guid
SELECT @imageSort = ISNULL(max(imageSort) + 1, 1) FROM imageportal.images WHERE categoryId =  @categoryId AND @product = @product


UPDATE ImagePortal.Images 
SET 
    isActive = 0,
	imageSort = @imageSort
    WHERE guid = cast(@guid  as uniqueidentifier)


SET @imageSort = 2

INSERT INTO @linkRows
	SELECT imageId, product
		FROM imageportal.images 
		WHERE isActive = 1 
		AND categoryId = @categoryId
		AND product = @product
		AND imageType > 2
		GROUP BY imageId, product
		ORDER BY imageId

select * from @linkRows

WHILE (SELECT COUNT(*) FROM @linkRows) > 0
BEGIN
	SELECT TOP 1 @imageId = imageId,  @product = product
	FROM @linkRows
	INSERT INTO @linkRowsUpdate
	VALUES (@imageId, @product, @imageSort)

	SET @imageSort = @imageSort + 1
	DELETE FROM @linkRows WHERE imageId = @imageId
END

select * from @linkRowsUpdate 

UPDATE i
SET imageSort = t.imageSort
FROM imageportal.images i, @linkRowsUpdate t
WHERE i.imageId = t.imageId