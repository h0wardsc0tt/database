USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_NewCategory]    Script Date: 8/21/2016 3:02:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_NewCategory](
	@categoryDescription NVARCHAR(250),
	@categoryName NVARCHAR(150),
	@image IMAGE,
	@imageName NVARCHAR(50),
	@imagePrefix NVARCHAR(4)
)AS
DECLARE @categorySort INT
DECLARE @categoryId INT

SELECT  @categorySort = max(categorySort) + 1 from ImagePortal.categories where isActive = 1
INSERT INTO ImagePortal.categories
(categoryName, CategoryDescription, categorySort)
VALUES(@categoryName, @categoryDescription, @categorySort)

SET @categoryId = @@IDENTITY

INSERT INTO ImagePortal.images
(categoryid, image, imagePrefix, imageType, imageName, ImageDescription, ImageSort, Product, ProductOrder)
VALUES(@categoryId, @image, @imagePrefix, 1, @imageName ,@categoryDescription,0, 1 ,1)



