USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateProductLink]    Script Date: 8/21/2016 3:07:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateProductLink](
	@image IMAGE,
	@guid NVARCHAR(150),
	@imageType int,
	@imageAnchorText VARCHAR(150),
	@imageName VARCHAR(50),
	@imagePrefix VARCHAR(4)
)AS

UPDATE ImagePortal.Images 
SET 
    image = @image,
    imageType = @imageType, 
    imageAnchorText = @imageAnchorText, 
    imageName = @imageName, 
    imagePrefix = @imagePrefix
    WHERE guid = cast(@guid  as uniqueidentifier)


