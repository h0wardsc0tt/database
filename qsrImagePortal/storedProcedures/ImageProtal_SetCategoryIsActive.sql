USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetCategoryIsActive]    Script Date: 8/21/2016 3:04:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetCategoryIsActive](
	@categoryGuid NVARCHAR(150)
)AS

DECLARE @categorySort INT
DECLARE @categoryId INT
DECLARE @categoryRows TABLE (categoryId  INT)
DECLARE @categoryRowsUpdate TABLE (categoryId  INT, categorySort INT)

SELECT @categorySort = ISNULL(max(categorySort) + 1, 1)
FROM ImagePortal.categories

UPDATE ImagePortal.categories
SET isActive = 0, categorySort =  @categorySort
WHERE CategoryGuid = @categoryGuid

SET @categorySort = 1
DECLARE @rowId INT

INSERT INTO @categoryRows
	SELECT categoryId
		FROM imageportal.categories 
		WHERE isActive = 1
		GROUP BY categoryId
		ORDER BY categoryId

WHILE (SELECT COUNT(*) FROM @categoryRows) > 0
BEGIN
	SELECT TOP 1 @categoryId = categoryId
	FROM @categoryRows
	INSERT INTO @categoryRowsUpdate
	VALUES (@categoryId ,@CategorySort)

	SET @categorySort = @categorySort + 1
	DELETE FROM @categoryRows WHERE categoryId = @categoryId
END

UPDATE c 
SET categorySort = t.categorySort
FROM imageportal.categories c, @categoryRowsUpdate t
WHERE c.categoryId = t.categoryId AND c.isActive = 1

