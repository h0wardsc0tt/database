USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SetCategoryToActive]    Script Date: 8/21/2016 3:04:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SetCategoryToActive](
	@CategoryGuid NVARCHAR(150)
)AS

DECLARE @categorySort INT

SELECT @categorySort = ISNULL(max([CategorySort]) + 1, 1) FROM imageportal.categories WHERE isActive = 1

UPDATE imageportal.categories set isActive = 1, categorySort = @categorySort WHERE CategoryGuid = @CategoryGuid




