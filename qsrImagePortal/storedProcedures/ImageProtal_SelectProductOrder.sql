USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_SelectProductOrder]    Script Date: 8/21/2016 3:03:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_SelectProductOrder](
	@categoryGuid NVARCHAR(150)
)
AS

DECLARE @categoryId INT

SELECT  @categoryId = categoryId FROM imagePortal.categories c WHERE c.CategoryGuid = @categoryGuid

DECLARE @productCount INT

select @productCount = count(*) from ImagePortal.images where imageType = 2 and isActive = 1 and categoryId = @categoryId
group by categoryId

select c.CategoryDescription, i.product, i.productOrder, i.ImageName, replace(i.imageDescription,' (image only)','') as imageDescription, @productCount as count
from ImagePortal.images i 
INNER JOIN ImagePortal.categories c on c.CategoryGuid = @categoryGuid
where i.imageType = 2 and i.isActive = 1 and i.categoryId = @categoryId
order by productOrder