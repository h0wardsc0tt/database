USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_AddNewProductLink]    Script Date: 8/21/2016 3:01:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_AddNewProductLink](
	@guid NVARCHAR(150),
	@imageName  NVARCHAR(50),
	@image IMAGE,
	@imagePrefix NVARCHAR(4)
)AS

DECLARE @categoryId INT
DECLARE @Product INT
DECLARE @ProductOrder INT
DECLARE @anchorText NVARCHAR(150)
DECLARE @imageDescription  NVARCHAR(250)
DECLARE @imageType  INT
DECLARE @imageSort INT

	SELECT @categoryId = categoryId FROM ImagePortal.images WHERE guid = @guid and isActive = 1

	SELECT @anchorText = LinkDescription, @imageType = ImageType FROM [HME].[dbo].[ImagePortal.link_types]
	WHERE ImagePrefix = @imagePrefix
	
	SELECT @productOrder = Product, @product = Product, @imageDescription = imageDescription
	FROM ImagePortal.images WHERE guid = @guid and isActive = 1

	SELECT @imageSort = max(imageSort) + 1
	FROM ImagePortal.images WHERE categoryId = @categoryId AND product = @product AND isActive = 1


insert into ImagePortal.Images 
(
	CategoryId
	,Product
	,ProductOrder
	,ImageType
	,ImageAnchorText
	,ImageName
	,ImageDescription
	,ImageSort
	,Image
	,ImagePrefix
)

VALUES
(
	@categoryId,
	@product,
	@productOrder,
	@imageType,
	@anchorText,
	@imageName,
	@imageDescription,
	@imageSort,
	@image,
	@imagePrefix
)
