USE [HME]
GO
/****** Object:  StoredProcedure [dbo].[ImageProtal_UpdateImage]    Script Date: 8/21/2016 3:06:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[ImageProtal_UpdateImage](
	@image IMAGE,
	@guid NVARCHAR(150)
)AS

UPDATE ImagePortal.images 
SET image = @image
WHERE guid = cast(@guid  as uniqueidentifier)


